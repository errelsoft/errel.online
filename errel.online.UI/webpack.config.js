const path = require('path');
 
const CleanPlugin = require('clean-webpack-plugin');
const ExtractCssPlugin = require('mini-css-extract-plugin');
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin');
 
const sourceDir = path.resolve(__dirname, './src/');
const buildDir = path.resolve('../errel.online/ClientUI/build');
  
const babelLoader = {
   loader: 'babel-loader',
   options: {
       presets: [['@babel/preset-env', { modules: false, useBuiltIns: 'usage', corejs: 3 }]],
       plugins: [
           '@babel/plugin-proposal-object-rest-spread',
           ['@babel/plugin-proposal-decorators', { legacy: true }],
           '@babel/plugin-proposal-class-properties'
       ]
   }
};

module.exports = {
    experiments: { topLevelAwait: true },
    devtool: 'source-map',
    entry: {
        ui: './src/ui.js'
    },
    output: {
      path: buildDir,
      filename: '[name].js'
    },
    mode: 'development',
    module: {
        rules: [
  
            {
                test: /\.scss$/,
                use: [ExtractCssPlugin.loader, 'css-loader', 'sass-loader']
            },
  
            {
                exclude: /(node_modules|bower_components)/,
                include: sourceDir,
                test: /\.js?/,
                use: [{ ...babelLoader }]
            },
  
            {
                test: /\.css$/,
                use: [ExtractCssPlugin.loader, 'css-loader']
            },
  
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
	resolve: {
      extensions: ['.js', '.vue'], // allows us to omit .vue
      alias: {
        '@app': path.resolve(__dirname, 'src/app/')
      }
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendors: {
                    chunks: 'all',
                    name: 'vendor',
                    test: /[\\/]node_modules[\\/]/
                }
            }
        },
    },
    plugins: [
        // Deletes build directory before rebuilds.
        new CleanPlugin({
            cleanOnceBeforeBuildPatterns: [`${buildDir}/**`],
            dangerouslyAllowCleanPatternsOutsideProject: true
        }),
  
        // Extract CSS from bundles, and compile them into a single CSS file
        new ExtractCssPlugin({
            filename: '[name].min.css'
        }),
  
        // Clone any other rules you have defined and apply them to the corresponding language blocks in .vue files.
        
        new VueLoaderPlugin(),

        // new HtmlWebpackPlugin({
        //     filename: '_index_example.html',
        //     minify: false,
        //   })
    ],
 };