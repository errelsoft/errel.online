import {withAuthorization} from "@app/api/helpers";

const SYMBOL_BASE_URL = Symbol('baseUrl');

class ApiBase {
    /**
     * @param {string} baseUrl
     */
    constructor(baseUrl){
        //Ensure a proper URL for ease of use
        if(!baseUrl.endsWith('/')){
            baseUrl += '/';
        }
        
        this[SYMBOL_BASE_URL] = baseUrl;
    }

    get baseUrl(){
        return this[SYMBOL_BASE_URL];
    }

    /**
     * When available, combines the given endpoint with the baseUrl
     * @param {string?} endpoint
     * @returns {string}
     */
    getRelativeEndpoint(endpoint){
        return `${this.baseUrl}${endpoint || ''}`;
    }
    
    /**
     * Perform a get request to the given API endpoint
     * @param {string} endpoint
     * @param {RequestInit?} init
     * @returns {Promise<DnxResponse>}
     */
    async get(endpoint, init){
        //TODO: Support RequestInfo
        init = withAuthorization(init);
        init.method = 'GET';

        init.headers = init.headers || {};
        
        const response = await fetch(
            this.getRelativeEndpoint(endpoint),
            init
        );

        return await response.json();
    } 

    /**
     * Perform a post request to the given API endpoint
     * @param {string} endpoint
     * @param {RequestInit?} init
     * @param {*} content
     * @returns {Promise<DnxResponse>}
     */
    async post(endpoint, init, content){
        init = withAuthorization(init);
        init.method = 'POST';

        init.headers = init.headers || {};
        init.headers['Content-Type'] = 'application/json';
        init.body = JSON.stringify(content);
        
        const response = await fetch(
            this.getRelativeEndpoint(endpoint),
            init
        );
        
        return await response.json();
    }
}

export default ApiBase;