import store from "@app/store";

/**
 * @param {RequestInit?} options
 * @returns {RequestInit}
 */
const withAuthorization = (options) => {
    const token = store.state.login.token;
    
    options = options || {};

    //Only modify our options when a token is actually present
    if(token){
        //Ensure our options contains a headers object
        if(!options.headers){
            options.headers = {};
        }

        //Add our token as bearer
        options.headers.Authorization = `Bearer ${token}`;   
    }
    
    return options;
};

export {
    withAuthorization
};
