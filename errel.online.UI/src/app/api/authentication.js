import ApiBase from "@app/api/apiBase";

/**
 * API for actions relating to user authentication
 */
class AuthenticationApi extends ApiBase {
    constructor() {
        super('/api/authentication/');
    }

    /**
     * @param {{ username: string, password: string }} model
     * @returns {Promise<DnxResponse>}
     */
    loginWithCredentials(model){
        return this.postJson('loginWithCredentials', undefined, model);
    }

    /**
     * Validates whether the current user is authenticated or not
     * @returns {Promise<boolean>}
     */
    async validateCurrentAuthenticationStatus() {
        const response = await this.get(`validate`);
        return response.statusCode !== 401;
    }
}

export default new AuthenticationApi();