import {createRouter, createWebHistory} from 'vue-router';
import {defineAsyncComponent} from 'vue';

const routes = [
    {
        path: '/home', 
        meta: { allowUnauthenticated: true }, 
        component: defineAsyncComponent(() => import('@app/components/Home.vue')),
    }];

const history = createWebHistory();

const router =  createRouter({
    // mode: 'history',
    history,
    routes,
    scrollBehavior (to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      } else {
        return { x: 0, y: 0 };
      }
    }
});

router.afterEach((to, from) => {  
  //store.commit('setLoaderVisible', false)
});

router.beforeEach((to, from, next) => {
   
    if (to.meta && to.meta.allowUnauthenticated !== true) {
        console.log('Auth required, checking status');
       
        store.dispatch('login/verifyLoginState').then(response => {
            if(response === true) { //store.state.login && store.state.login.loggedIn) {
                console.log('verifyLoginState: user logged in ', response);
                return next();            
            }   
            console.warn('User not logged in. Dispatch failed to validate');
            return next('/login');
          }).catch(error => {
            console.log('Authentication error', error);
            return next('/login');
          });
    } else {
      next();
    }
  });


export default router;