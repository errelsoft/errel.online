import {createApp, defineComponent} from 'vue';
import router from '@app/router';
import store from '@app/store';

export default () => {
    const newRootComponent = defineComponent({
        name: 'App'
    })
    
    debugger;
    const app = createApp(newRootComponent);
    
    app.use(store)
       .use(router)
       .mount('#application');
};