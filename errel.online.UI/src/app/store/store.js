import {createStore} from "vuex";

import createPersistedStatePlugin from './store-persistence';
import cookieBasedTokenStorePlugin from "./plugins/cookie-token-storage-plugin";


// attempt to sync session storage between tabs
//import storageSync from './util/tab_sync'
//storageSync(['vuex'])

export default createStore(
    {
        strict: true,
        plugins: [
            createPersistedStatePlugin(),
            cookieBasedTokenStorePlugin
        ],
        modules: {
            
        }
    }
)