//https://github.com/robinvdvleuten/vuex-persistedstate
import createPersistedState from "vuex-persistedstate";

var storageHandler = typeof(sessionStorage) !== 'undefined' ? sessionStorage : {
    // if sessionStorage is not available mock an empty one for now
    // we could switch to a sessionCookie later
    getItem: (key) => {},
    setItem:(key, value) => {},
    removeItem:(key) => {}
} 


export default () => {
    return createPersistedState({
        //We only want to persist specific values required for cross-tab browsing to work
        paths: ['login.token'],
        
        storage: {
          getItem: (key) => storageHandler.getItem(key),
          setItem: (key, value) => storageHandler.setItem(key, value),
          removeItem: (key) => storageHandler.removeItem(key)
        },
      })
}