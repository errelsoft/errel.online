import { Cookie, SameSiteMode } from "@app/utils/cookie";

const cookieName = 'errel.online.token';

const setTokenCookie = (token) => {
    //sub1.site.tld -> site.tld
    //site.tld -> site.tld
    //localhost -> localhost
    const cookieDomain = document.location.hostname.split('.')
        .filter((_, idx, {length}) => length - idx <= 2)
        .join('.');

    //https://stackoverflow.com/a/24103596
    const cookie = new Cookie(cookieName, token);
    cookie.setExpiresFromDays(1);
    cookie.domain = cookieDomain;
    cookie.sameSite = SameSiteMode.Lax;
    cookie.secure = true;
    
    document.cookie = cookie.toString();
}

const deleteTokenCookie = () => {
    const cookie = Cookie.Expired(cookieName);
    cookie.sameSite = SameSiteMode.Lax;

    document.cookie = cookie.toString();
}

const cookieBasedTokenStorePlugin = store => {
    store.subscribe((mutation) => {
        if(mutation.type !== 'login/setToken'){
            return;
        }
        
        //Empty payload implies logout
        if(mutation.payload){
            setTokenCookie(mutation.payload);
        } else{
            deleteTokenCookie();
        }
    });
}

export default cookieBasedTokenStorePlugin;