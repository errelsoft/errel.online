//import forgotPassword from "./forgotPassword";
import authApi from '@app/api/authentication';

//Defined in backend
const TFA_CLAIM_NAME = 'TfaMarkerClaim';
const NAME_IDENTIFIER_CLAIM_NAME = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier';

const createLoginActionHandler = (callApiWithPayload) => {
    return async (context, payload) => {
        try{
            const response = await callApiWithPayload(payload);

            if(response.access_token) {
                context.commit('setToken', response.access_token);
            }

            return response;
        } catch (e) {
            console.error(e);
        }
    };
};

const getTokenClaims = token => {
    //Header.Payload (claims).Signature
    const tokenClaimsBase64String = token.split('.')[1];
    
    //Our token is Base64 encoded JSON
    const tokenClaimsJsonString = atob(tokenClaimsBase64String);
    return JSON.parse(tokenClaimsJsonString);
};

export default LoginModule  = {
    namespaced: true,
    modules: {
        //forgotPassword
    },
    state: {
        // loggedIn: false,
        token: undefined
    },
    getters: {
        loggedIn(state, context){
            return state.token !== undefined;// && !context.isTfaToken;
        },
        isTfaConfirm(state){
            if(!state.token){
                return false;
            }
            
            return getTokenClaims(state.token)[TFA_CLAIM_NAME] === 'false';
        },
        isTfaToken(state){
            if(!state.token){
                return false;
            }

            return getTokenClaims(state.token).hasOwnProperty(TFA_CLAIM_NAME);
        },
        userId(state){
            if(!state.token){
                return undefined;
            }
            
            return getTokenClaims(state.token)[NAME_IDENTIFIER_CLAIM_NAME];
        }
    },
    mutations: {
        setToken(state, value){
            state.token = value;
        }
    },
    
    actions: {
        logout(context){
            context.commit('setToken', undefined);
        },
        
        loginWithCredentials: createLoginActionHandler(
            payload => authApi.loginWithCredentials(payload)
        ),
        asyncAction(context) {
            // actions may return a promise
            //context.commit('somestate', 1)
            return new Promise((resolve, reject) => {
                // use timout to simlulate http call
                setTimeout(async () => resolve(), 1000);
            })
        },

        async verifyLoginState(context){
            const token = context.state.token;
            
            if(!token) {
                return false;
            }
            
            const isValid = await authApi.validateCurrentAuthenticationStatus();
           
            if(!isValid){
                context.commit('setToken', undefined);
            }
            
            return isValid;
        }
    }
};
