import {createResetMutation} from "@app/store/util/reset_state";
import areaApi from "@app/api/area";
import _ from 'lodash';

const GET_INITIAL_STATE = () => ({
   
});

export default AreaModule = {
    namespaced: true,
    state: GET_INITIAL_STATE(),
    mutations: {

    },
    actions: {

    }
};