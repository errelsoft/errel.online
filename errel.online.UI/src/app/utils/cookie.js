const SameSiteMode = {
    None: Symbol('None'),
    Lax: Symbol('Lax'),
    Strict: Symbol('Strict')
}

class Cookie {
    /**
     * Create a new cookie
     * @param {string} name Cookie name
     * @param {string} value Cookie value
     */
    constructor(name, value) {
        this.name = name;
        this.value = value;
        this.path = '/';
        
    }

    /**
     * Create an expired cookie
     * @param {string} name Cookie name
     * @returns {Cookie} Expired cookie
     */
    static Expired(name){
        const cookie = new Cookie(name, '');
        cookie.setExpiresFromDays(-999);
        return cookie;
    }

    /**
     * @returns {string}
     */
    get name(){
        return this._name;
    }

    /**
     * @param {string} value
     */
    set name(value){
        this._name = value;
    }

    /**
     * @returns {string}
     */
    get value(){
        return this._value;
    }

    /**
     * @param {string} value
     */
    set value(value){
        this._value = value;
    }

    /**
     * @returns {Date|undefined}
     */
    get expires(){
        return this._expires;
    }

    /**
     * @param {Date|undefined} value
     */
    set expires(value){
        this._expires = value;
    }

    /**
     * @returns {string|undefined}
     */
    get domain(){
        return this._domain;
    }

    /**
     * @param {string|undefined} value
     */
    set domain(value){
        this._domain = value;
    }

    /**
     * @returns {string}
     */
    get path(){
        return this._path;
    }

    /**
     * @param {string} value
     */
    set path(value){
        this._path = value;
    }

    /**
     * @returns {Symbol|undefined}
     */
    get sameSite(){
        return this._sameSite;
    }

    /**
     * @param {Object|undefined} value
     */
    set sameSite(value){
        this._sameSite = value;
    }

    /**
     * @returns {Boolean|undefined}
     */
    get secure(){
        return this._secure;
    }

    /**
     * @param {Boolean|undefined} value
     */
    set secure(value){
        this._secure = value;
    }

    /**
     * @returns {Boolean|undefined}
     */
    get httpOnly(){
        return this._httpOnly;
    }

    /**
     * @param {Boolean|undefined} value
     */
    set httpOnly(value){
        this._httpOnly = value;
    }

    /**
     * Set the cookie expires property to the given amount of days in the future
     * @param {Number} days
     */
    setExpiresFromDays(days){
        //days * hours * minutes * seconds * milliseconds
        const daysInTime = days * 24 * 60 * 60 * 1000;

        const d = new Date();

        d.setTime(d.getTime() + daysInTime);

        this.expires = d;
    }

    /**
     * Stringify to JavaScript cookie
     * @returns {string} Stringified cookie
     */
    toString(){
        const cookieParts = [`${this.name}=${this.value}`];

        if(this.expires){
            cookieParts.push(`expires=${this.expires.toUTCString()}`);
        }

        if(this.domain){
            cookieParts.push(`domain=${this.domain}`);
        }

        if(this.path){
            cookieParts.push(`path=${this.path}`)
        }

        if(this.sameSite){
            let sameSiteModeStr = undefined;

            switch(this.sameSite){
                case SameSiteMode.None:
                    sameSiteModeStr = 'None';
                    break;
                case SameSiteMode.Lax:
                    sameSiteModeStr = 'Lax';
                    break;
                case SameSiteMode.Strict:
                    sameSiteModeStr = 'Strict';
                    break;
            }

            if(sameSiteModeStr){
                cookieParts.push(`SameSite=${sameSiteModeStr}`);
            }
        }

        if(this.secure === true){
            cookieParts.push('Secure');
        }

        if(this.httpOnly === true){
            cookieParts.push('HttpOnly');
        }

        return cookieParts.join(';');
    }
}

export {
    SameSiteMode,
    Cookie
}