﻿using System.Linq;

namespace errel.online.MiddleWare
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.FileProviders;
    using Microsoft.Extensions.Options;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    
    using IWebHostEnvironment = Microsoft.AspNetCore.Hosting.IWebHostEnvironment;
    public static class CatchAllIndexFilesMiddleWareExtensions
    {
        public static void UseCatchAllIndexFilesMiddleWare(this IApplicationBuilder app, Action<ICatchAllIndexFileBuilder> configuration)
        {
            var env = app.ApplicationServices.GetRequiredService<IWebHostEnvironment>();

            var optionsProvider = app.ApplicationServices.GetService<IOptions<CatchAllIndexFilesMiddleWareOptions>>();
            if (optionsProvider == null) 
                return;

            var options = new CatchAllIndexFilesMiddleWareOptions(optionsProvider.Value);

            var spabuilder = new CatchAllIndexFileBuilder(app, options);
            configuration?.Invoke(spabuilder);
            
            var catchAllMiddleware = new CatchAllIndexFileMiddleWare(env, options);
            app.Use(catchAllMiddleware.HandleRequest);
        }
    }
    public class CatchAllIndexFilesMiddleWareOptions
    {
        public Regex ResourcePathMatcher { get; set; } = new Regex(@"\.(js|json|html|gif|jpg|png|css|scss|map)$", RegexOptions.Compiled);
        public Regex ExcludedPathMatcher { get; set; } = new Regex(@"^/(api|build|app)/.*", RegexOptions.Compiled);

        public string IndexFileName { get; set; } = "index.html";
        public IFileProvider FileProvider { get; internal set; }

        public Dictionary<string,string> ContentReplacements {get;set;} = new();

        public CatchAllIndexFilesMiddleWareOptions()
        {
        }

        public CatchAllIndexFilesMiddleWareOptions(CatchAllIndexFilesMiddleWareOptions options)
        {
            if (options == null) 
                return;

            ResourcePathMatcher = options.ResourcePathMatcher;
            ExcludedPathMatcher = options.ExcludedPathMatcher;
            IndexFileName = options.IndexFileName;
            FileProvider = options.FileProvider;
        }

    }
    public interface ICatchAllIndexFileBuilder
    {
        /// <summary>
        /// The <see cref="IApplicationBuilder"/> representing the middleware pipeline
        /// in which the SPA is being hosted.
        /// </summary>
        IApplicationBuilder ApplicationBuilder { get; }

        /// <summary>
        /// Describes configuration options for hosting a SPA.
        /// </summary>
        CatchAllIndexFilesMiddleWareOptions Options { get; }
    }
    public class CatchAllIndexFileBuilder : ICatchAllIndexFileBuilder
    {
        public IApplicationBuilder ApplicationBuilder { get; }

        public CatchAllIndexFilesMiddleWareOptions Options { get; }

        public CatchAllIndexFileBuilder(IApplicationBuilder applicationBuilder, CatchAllIndexFilesMiddleWareOptions options)
        {
            ApplicationBuilder = applicationBuilder
                ?? throw new ArgumentNullException(nameof(applicationBuilder));

            Options = options
                ?? throw new ArgumentNullException(nameof(options));
        }
    }


    public class CatchAllIndexFileMiddleWare
    {
        private readonly Regex _resourcePathRegex;
        private readonly Regex _excludedPathsRegex;
        private byte[] _indexFileData;

        private IWebHostEnvironment Env { get; }
        private DateTime IndexFileModified { get; set; }
        private string IndexFileName { get; set; }
        private IFileProvider FileProvider { get; }
    	private Dictionary<string,string> ContentReplacements {get;}

        public CatchAllIndexFileMiddleWare(IWebHostEnvironment env, CatchAllIndexFilesMiddleWareOptions options)
        {
            Env = env;
            FileProvider = options.FileProvider ?? Env.WebRootFileProvider;
            IndexFileName = options.IndexFileName ?? "index.html";
            _resourcePathRegex = options.ResourcePathMatcher; 
            _excludedPathsRegex = options.ExcludedPathMatcher;
            ContentReplacements = options.ContentReplacements;
        }

        private byte[] GetIndexFile()
        {
            if (IndexFileModified != default && !(DateTime.UtcNow.Subtract(IndexFileModified).TotalSeconds > 30))
                return _indexFileData;

            var fi = FileProvider.GetFileInfo(IndexFileName);
            var shouldReload = fi.Exists;

            shouldReload = shouldReload && (IndexFileModified == default || fi.LastModified > IndexFileModified);
            
            if (!shouldReload)
                return _indexFileData;

            IndexFileModified = fi.LastModified.UtcDateTime;

            using var ms = new MemoryStream();
            using var s = fi.CreateReadStream();
                    
            s.CopyTo(ms);
            _indexFileData = ms.ToArray();

            if (ContentReplacements.Count <= 0) 
                return _indexFileData;

            var txt = Encoding.UTF8.GetString(_indexFileData);

            txt = ContentReplacements.Aggregate(txt, (current, kv) => current.Replace(kv.Key, kv.Value));

            _indexFileData = Encoding.UTF8.GetBytes(txt);
            return _indexFileData;
        }

        public async Task HandleRequest(Microsoft.AspNetCore.Http.HttpContext ctx, Func<Task> next)
        {
            if (ctx.Request.Path.HasValue)
            {
                var path = ctx.Request.Path.Value ?? String.Empty;
                
                var isExcluded = _excludedPathsRegex.IsMatch(path) || _resourcePathRegex.IsMatch(path);
                if (!isExcluded)
                {
                    var fileData = GetIndexFile();
                    if (fileData != null)
                    {
                        ctx.Response.StatusCode = 200;

                        ctx.Response.Headers.Add("Expires", DateTime.UtcNow.AddYears(-10).ToString("r"));
                        ctx.Response.Headers.Add("Last-Modified", IndexFileModified.ToString("r"));
                        ctx.Response.Headers.Add("Cache-Control", "must-revalidate");
                        ctx.Response.Headers.Add("Content-Type", "text/html");
                        ctx.Response.Headers.Add("Content-Length", $"{fileData.Length}");

                        await ctx.Response.Body.WriteAsync(fileData).ConfigureAwait(false);
                        return;
                    }
                }
            }
            await next().ConfigureAwait(false);
        }
    }
}
