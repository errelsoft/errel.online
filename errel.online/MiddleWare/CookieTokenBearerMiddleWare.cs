﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace errel.online.MiddleWare
{
    public static class CookieTokenBearerMiddlewareExtensions
    {
        public static void UseCookieTokenBearerMiddleware(this IApplicationBuilder app, string cookieName)
        {
            var middleWare = new CookieBearerAuthenticationMiddleWare(cookieName);
            app.Use(middleWare.InvokeAsync);
        }
    }


    public class CookieBearerAuthenticationMiddleWare
    {
        private readonly string _cookieName;
        
        public CookieBearerAuthenticationMiddleWare(string cookieName)
        {
            _cookieName = cookieName;
        }
        
        public async Task InvokeAsync(HttpContext context, Func<Task> next)
        {
            //https://stackoverflow.com/a/39386631
            
            if (context.Request.Cookies.TryGetValue(_cookieName, out var token))
            {
                if (!context.Request.Headers.ContainsKey(HeaderNames.Authorization))
                {
                    context.Request.Headers[HeaderNames.Authorization] = $"Bearer {token}";   
                }
            }

            await next().ConfigureAwait(false);
        }
    }
}
