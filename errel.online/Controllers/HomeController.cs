﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace errel.online.Controllers
{
    [ApiController]
    [Route("api/home")]
    [Authorize]
    public class HomeController : ControllerBase
    {
        [HttpGet("hai")]
        public IActionResult Index()
        {
            return Ok("hai");
        }
    }
}
