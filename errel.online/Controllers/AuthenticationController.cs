﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using errel.online.Configuration;
using errel.online.Models;
using errel.online.Util.Attributes.Dnx.Api.Core.Util.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace errel.online.Controllers
{
    [ApiController]
    [Route("api/authentication")]
    public class AuthenticationController : ControllerBase
    {
        private readonly JwtOptions _jwtOptions;

        public AuthenticationController(IOptions<JwtOptions> jwtOptions)
        {
            _jwtOptions = jwtOptions.Value;
        }

        public override OkObjectResult Ok(object value)
        {
            Response.Headers.Add("Cache-Control", "no-store");
            Response.Headers.Add("Pragma", "no-cache");
            return base.Ok(value);
        }

        [HttpPost("login")]
        [NamingPolicy(NamingPolicyFormat.Snake)]
        public IActionResult LoginWithCredentials([FromBody] LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new UserModel
            {
                Id = Guid.NewGuid(),
                UserName = "Errel"
            };

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var expires = DateTime.UtcNow.AddDays(1);
            var token = CreateUserToken(claims,expires);
            return TokenResponse(token,expires);
        }

        [Authorize]
        [HttpGet("validate")]
        public IActionResult Validate() => Ok();

        private IActionResult TokenResponse(string token, DateTime expires)
        {
            return Ok(new
            {
                TokenType = "Bearer",
                AccessToken = token,
                ExpiresIn = (expires - DateTime.UtcNow).TotalSeconds
            });
        }

        private string CreateUserToken(Claim[] claims, DateTime expires)
        { 
            //Add our claims to our user so they can be used immediately
            User.AddIdentities(new []
            {
                new ClaimsIdentity(claims)
            });

            var credentials = new SigningCredentials(_jwtOptions.GetSigningKey(), SecurityAlgorithms.HmacSha256);
            
            var token = new JwtSecurityToken(
                _jwtOptions.Issuer, 
                _jwtOptions.Audience,
                claims,
                expires: expires,    
                signingCredentials: credentials
            );
            
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
