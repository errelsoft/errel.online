using System;
using System.IO;
using System.Text.Json;
using errel.online.Configuration;
using errel.online.MiddleWare;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace errel.online
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddControllers().AddJsonOptions(options => 
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                options.JsonSerializerOptions.IgnoreNullValues = true;
            });
            
            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status302Found;
                options.HttpsPort = 443;
            });

            //Set up JWT authentication.
            ConfigureJwtBearerAuthentication(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseCatchAllIndexFilesMiddleWare(builder => {
                // We want a modern SPA, so need to make sure requests end at the index html file
                // exclude some url's like /api/
                builder.Options.IndexFileName = "index.html";
                builder.Options.FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "ClientUI"));
                builder.Options.ContentReplacements["${datetime.timestamp}"] = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                builder.Options.ContentReplacements["${base_href}"] = "/";
                builder.Options.ContentReplacements["${api_base_url}"] = "/api";

            });

            app.UseRouting();

            app.UseCookieTokenBearerMiddleware("errel.online.token");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints
                    .MapControllerRoute(
                        name: "default",
                        pattern: "{api}/{controller}/{action}/{id?}");
            });
        }

        public void ConfigureJwtBearerAuthentication(IServiceCollection services)
        {
            var jwtSection = Configuration.GetSection("authentication:jwt");
            var options = jwtSection.Get<JwtOptions>();

            //todo Add 2FA

            services.Configure<JwtOptions>(jwtSection);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opts =>
                {
                    //Set clock skew to 30 seconds. This is to handle any differences in time
                    //between the authorization server and the API (default is 5 minutes).
                    opts.TokenValidationParameters = new TokenValidationParameters
                    {
                        ClockSkew = new TimeSpan(0, 0, 30)
                    };

                    opts.TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerSigningKey = options.GetSigningKey(),
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidIssuer = options.Issuer,
                        ValidAudience = options.Audience
                    };
                });
        }
    }
}
