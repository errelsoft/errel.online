﻿using errel.online.Util.NamingPolicies;

namespace errel.online.Util.Attributes
{
    using System;
    using System.Text.Json;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.AspNetCore.Mvc.Formatters;

    namespace Dnx.Api.Core.Util.Attributes
    {
        /// <summary>
        /// Attribute to change the default context JSON naming policy to a custom policy.
        /// </summary>
        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
        public class NamingPolicyAttribute :  ActionFilterAttribute
        {
            private readonly NamingPolicyFormat _formatName;

            public NamingPolicyAttribute(NamingPolicyFormat formatName)
            {
                _formatName = formatName;
            }

            public override void OnActionExecuted(ActionExecutedContext context)
            {
                if (context?.Result == null)
                {
                    return;
                }

                var options = new JsonSerializerOptions
                {
                    //Add more custom naming policies here.
                    PropertyNamingPolicy = _formatName switch
                    {
                        NamingPolicyFormat.Snake => new JsonSnakeCaseNamingPolicy(),
                        _ => JsonNamingPolicy.CamelCase
                    }
                };

                var formatter = new SystemTextJsonOutputFormatter(options); //Create formatter.

                //Replace default formatter on context.
                (context.Result as Microsoft.AspNetCore.Mvc.OkObjectResult)?.Formatters.Add(formatter);
            }
        }

        public enum NamingPolicyFormat
        {
            Snake
        }
    }

}
