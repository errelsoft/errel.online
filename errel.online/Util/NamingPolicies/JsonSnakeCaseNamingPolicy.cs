﻿using System;
using System.Text;
using System.Text.Json;

namespace errel.online.Util.NamingPolicies
{
    public class JsonSnakeCaseNamingPolicy : JsonNamingPolicy
    {
        public override string ConvertName(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName)) return propertyName;
            var sb = new StringBuilder(propertyName.Length + 5);
            var name = propertyName.AsSpan();
            
            for (var i = 0; i < name.Length; i++)
            {
                if (i==0) sb.Append(Char.ToLower(name[i]));
                else if (Char.IsUpper(name[i]) && i > 0)
                    sb.Append("_").Append(Char.ToLower(name[i]));
                else sb.Append(name[i]);
            }

            return sb.ToString();
        }
    }
}
