﻿using System;

namespace errel.online.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
    }
}
