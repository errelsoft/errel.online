﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace errel.online.Configuration
{
    public class JwtOptions
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }

        public SecurityKey GetSigningKey()
        {
            return new SymmetricSecurityKey(
                Encoding.ASCII.GetBytes(Secret)
            );
        }
    }
}
